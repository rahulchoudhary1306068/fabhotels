var path="http://localhost/ClassZoo/public/api/";
//var path="http://classzoo.trionprojects.com/api/";


  angular.module("KendoDemos", [ "kendo.directives" ])
          .controller("MyCtrl", function($scope){
             
          });
function searchController($scope,$http) {
   $scope.searchByChapter=function(cha){
       window.location.href="http://localhost/ClassZoo/public/#/result?search_type=&chapter="+cha
   }
   
   $http.get(path+"classes/get-all-class-and-subjects")
    .success(function(response){
       $scope.classes = response.data;
       $scope.classes.value = "";
     });
     $http.get(path+"subjects/get-all-subjects")
    .success(function(response){
       $scope.countryNames=response.subject;
      
     });
     
     $http.get(path+"courses/get-all-featured-course")
    .success(function(response){
       $scope.featuredCourses=response.data;
     });
     $scope.showSearch=function(value){
        
    //    window.location.href="http://localhost/ClassZoo/public/result?search_type=&search="+value;
//      window.location.href="http://classzoo.trionprojects.com/result?search_type=&search="+value;
     }
     
    
}

function classesController($scope,$http) {     
   
   $http.get(path+"classes/get-all-classes")
    .success(function(response){
       $scope.classes = response.data;
     });
     
     var class_name=$("#class_name").val();
     $http.get(path+"classes/get-class-by-name/class_name/"+class_name)
     .success(function(response){
       $scope.chapterTopics = response.data;

     });
}

function coursesController($scope,$http) {     
    var course_id=  $("#course_id").val();
   
   $http.get(path+"courses/get-course-by-id/course_id/"+course_id)
    .success(function(response){
       $scope.courses = response.data;
     });
     
     $http.get(path+"courses/get-all-courses")
    .success(function(response){
        $scope.allCourses = response.data;
     });
     
       $http.get(path+"courses/get-all-course-subtopics/course_id/"+course_id)
    .success(function(response){
        $scope.courseSubtopics = response.data;
     });
     
     var class_schedule_id=  $("#class_schedule_id").val();
      $http.get(path+"class-schedules/class-schedule-by-course-id/class_schedule_id/"+class_schedule_id)
    .success(function(response){
        $scope.cs = response.data;
     });
     
      
     var class_name=  $("#class_name").val();
      $http.get(path+"class-schedules/class-schedule-by-class-name/class_name/"+class_name)
    .success(function(response){
        $scope.allClasses = response.data;
     });
}


function resultController($scope,$http,$location) {
    $param=$location.search();
  if($param.chapter){
   $http.get(path+"class-schedules/get-topic-by-chapter/chapter/"+$param.chapter)
    .success(function(response){
       $scope.c=response.data;
     });   
  }
  var s=  $("#search_val").val();
   $http.get(path+"search/search-data/search_value/"+s)
    .success(function(response){
       $scope.searchs = response.data;
     });
     
       $(function() {
                $("#slider-range").slider({
                        range: true,
                        min: 0,
                        max: 500,
                        values: [75, 300],
                        slide: function(event, ui) {
                            $("#amount").val("Rs" + ui.values[ 0 ] + " - Rs" + ui.values[ 1 ]);
                            $("#min_price").val(ui.values[0]);
                            $("#max_price").val(ui.values[1]);
                        },
                        stop: function(event, ui) {
                        $scope.filter();
                        }
                    });
                    $("#amount").val("Rs" + $("#slider-range").slider("values", 0) +
                            " - Rs" + $("#slider-range").slider("values", 1));
                });
           
            
 $scope.filter=function(){
     
     var filter=$("#filter_form").serialize();
      $http.get(path+"search/search-data?"+filter)
    .success(function(response){

                $scope.searchs = response.data;
                if(response.meta.code==404)
                {
                        $scope.notfound = [response.meta.message];
                }
                else{
                    $scope.notfound="";
                }
     });
     
 }
 
      $http.get(path+"instructors/get-all-instructors")
    .success(function(response){
       $scope.instructors = response.data;
     });
     
     
     $scope.addWishlist=function(){
         var wish=$("#wishlist_form").serialize();
         
          $http.post(path+"courses/wishlist/data=?"+wish ).success(function(data){
            
            if(data.meta.code==200)
                {
                    toastr.success('Wishlist Added Successfully');
                    $(".ng-scope").modal('hide');
                }
                 else if(data.meta.code==404)
                {
                    toastr.error('Already Exists!');
                    $(".ng-scope").modal('hide');
                }
        });
     }
    
}

function walletController($scope,$http) {
  
  var s=  $("#student_id").val();
   $http.get(path+"wallet/get-student-amount/id/"+s)
    .success(function(response){
       $scope.credits = response.data.credit;
       $scope.debits = response.data.debit;
       $scope.courses = response.data.course;
     });
  
}

function profileController($scope,$http) {
  
  $scope.addAmount=function(a){
//        $http.get(path+"wallet/add-amount/id/"+s)
//    .success(function(response){
//       $scope.credits = response.data.credit;
//     });
     var student_id=$("#student_id").val();
     $http({
    url: path+"wallet/add-amount/", 
    method: "POST",
    params: {student_id: student_id,credit_amount:a.amount}
 }) .success(function(response){
      toastr.success('Amount Added Successfully!');
     });
    }  
}

function wishlistController($scope,$http) {
  
  var s=  $("#student_id").val();
   $http.get(path+"student-wishlist/get-all-student-wishlist/id/"+s)
    .success(function(response){
       $scope.wishlists = response.data;
     });
    
}

function registerController($scope,$http) {
    
  $scope.register=function(student){
    //  $.data=atob(student)
        var ss=$("#signup_form").serialize();
        
        $http.post(path+"students/student-register/data=?"+ss ).success(function(data){
            
            if(data.meta.code==200)
                {
                    toastr.success('Register Successfully!');
                    $(".ng-scope").modal('hide');
                }
                 else if(data.meta.code==404)
                {
                    toastr.error('Already Exists!');
                    $(".ng-scope").modal('hide');
                }
        });
        
  }
}


function loginController($scope,$http) {
    
  $scope.register=function(student){
    //  $.data=atob(student)
        var ss=$("#signup_form").serialize();
        
        $http.post(path+"students/student-register/data=?"+ss ).success(function(data){
            
            if(data.meta.code==200)
                {
                    toastr.success('Register Successfully!');
                    $(".ng-scope").modal('hide');
                }
                 else if(data.meta.code==404)
                {
                    toastr.error('Already Exists!');
                    $(".ng-scope").modal('hide');
                }
        });
        
  }
}

function contactController($scope,$http) {
   $scope.mail=function(value){
       
       var ss=$("#contact_form").serialize();
        
        $http.post(path+"contact/mail/data=?"+ss ).success(function(data){
            
            if(data.meta.code==200)
                {
                    toastr.success('Mail Sent Successfully!');
                    $(".ng-scope").modal('hide');
                }
                 else if(data.meta.code==404)
                {
                    toastr.error('Oops. Mail Not Sent!');
                    $(".ng-scope").modal('hide');
                }
        });
     }
     
    
}
