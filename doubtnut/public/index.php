<?php

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

//to include Resque
require __DIR__.'/../vendor/autoload.php';
#require_once("../braintree/includes/braintree_init.php");
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
require __DIR__.'/../vendor/massivescale/celery-php/celery.php';
//use Razorpay\Api\Api;

use WindowsAzure\Common\ServicesBuilder;
use WindowsAzure\Common\ServiceException;
//use Predis\Redis;

//rzp_live_bDeZX3YRPJeThg
//RRXUUC0k2TlRbM7TlVFgh10dDVWuGn76

//$api = new Api("rzp_live_bDeZX3YRPJeThg","");
//$api->payment->fetch($id)->capture(array('amount'=>$amount)); // Captures a payment
//var_dump($api);
//exit;

require_once realpath(APPLICATION_PATH . '/../vendor/autoload.php');
// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';
#date_default_timezone_set('Asia/Kolkata');
// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);
$application->bootstrap()
            ->run();


