<?php
#session_start();
require __DIR__."/../vendor/autoload.php";

if(file_exists(__DIR__ . "/../.env")) {
    $dotenv = new Dotenv\Dotenv(__DIR__ . "/../");
    $dotenv->load();
}

//Braintree\Configuration::environment(getenv('sandbox'));
//Braintree\Configuration::merchantId(getenv('63w6kp4vbg8ktz8r'));
//Braintree\Configuration::publicKey(getenv('qthf7c2bw9c79nyt'));
//Braintree\Configuration::privateKey(getenv('cdde774a3d30316448fca010d565b04f'));
Braintree\Configuration::environment(getenv('BT_ENVIRONMENT'));
Braintree\Configuration::merchantId(getenv('BT_MERCHANT_ID'));
Braintree\Configuration::publicKey(getenv('BT_PUBLIC_KEY'));
Braintree\Configuration::privateKey(getenv('BT_PRIVATE_KEY'));
