<?php
class Application_Model_FabhotelsMapper {

	private $conn;

    public function __construct() {
    	
        $this->conn=mysqli_connect(HOST,USERNAME,PASSWORD,DATABASE);      
    }

    public function __destruct(){
        mysqli_close($this->conn);
    }

    public function getHotels($searchKey){

    	if(isset($searchKey))
    	{
    		$searchKey=mysqli_real_escape_string($this->conn,$searchKey);
    		$sql="SELECT * FROM property_details WHERE (locality LIKE '%".$searchKey."%' OR city LIKE '%".$searchKey."%' OR state LIKE '%".$searchKey."%' OR country LIKE '%".$searchKey."%' ) AND status=1";	
            
       		$result=mysqli_query($this->conn,$sql);
       		// Validating the Result
        	
            if (!$result) 
        	{
            	$ans=array();
                // returning empty array
            	return $ans;
        	}
        	else
        	{
        		$ans=array();
        		while($row=mysqli_fetch_assoc($result)){
       				$ans[] = $row;	
       			}
        		return $ans;
        	}        
    	}
    	else
    	{
    		$ans=array();
            // returning empty array
            return $ans;
    	}    
    }

    public function getAllHotels()
    {
        $sql="SELECT * FROM property_details";
        $result=mysqli_query($this->conn,$sql);
        if (!$result) 
        {
            $ans=array();
            // returning empty array
            return $ans;
        }
        else
        {
            $ans=array();
            while($row=mysqli_fetch_assoc($result)){
                $ans[] = $row;  
            }
            return $ans;
        }

    }


    public function getHotel($id){

        if(isset($id))
        {
            $id=mysqli_real_escape_string($this->conn,$id);
            $sql="SELECT * FROM property_details WHERE id = '$id'";   
            
            $result=mysqli_query($this->conn,$sql);
            // Validating the Result
            
            if (!$result) 
            {
                $ans=array();
                // returning empty array
                return $ans;
            }
            else
            {
                $ans=array();
                while($row=mysqli_fetch_assoc($result)){
                    $ans[] = $row;  
                }
                return $ans;
            }        
        }
        else
        {
            $ans=array();
            // returning empty array
            return $ans;
        }    
    }


    public function updateHotel($id,$name,$locality,$city,$state,$country,$status){

        if(isset($id) && isset($name) && isset($locality) && isset($city) && isset($state) && isset($country) && isset($status))
        {
            $id=mysqli_real_escape_string($this->conn,$id);
            $name=mysqli_real_escape_string($this->conn,$name);
            $locality=mysqli_real_escape_string($this->conn,$locality);
            $city=mysqli_real_escape_string($this->conn,$city);
            $state=mysqli_real_escape_string($this->conn,$state);
            $country=mysqli_real_escape_string($this->conn,$country);
            $status=mysqli_real_escape_string($this->conn,$status);

            $sql="UPDATE property_details set name='$name',locality='$locality',city='$city',state='$state',country='$country',status='$status' WHERE id='$id'";   
            
            $result=mysqli_query($this->conn,$sql);
            // Validating the Result
            
            if (!$result) 
            {
                $ans=null;
                // returning null
                return $ans;
            }
            else
            {
                $ans="updated";
                return $ans;
            }        
        }
        else
        {
            $ans=null;
            // returning null
            return $ans;
        }    
    }

    public function deleteHotel($id){

        if(isset($id))
        {
            $id=mysqli_real_escape_string($this->conn,$id);

            $sql="DELETE FROM property_details WHERE id='$id'";   
            
            $result=mysqli_query($this->conn,$sql);
            // Validating the Result
            
            if (!$result) 
            {
                $ans=null;
                // returning null
                return $ans;
            }
            else
            {
                $ans="deleted";
                return $ans;
            }        
        }
        else
        {
            $ans=null;
            // returning null
            return $ans;
        }    
    }

    public function createHotel($name,$locality,$city,$state,$country,$status){

        if(isset($name) && isset($locality) && isset($city) && isset($state) && isset($country) && isset($status))
        {
            
            $name=mysqli_real_escape_string($this->conn,$name);
            $locality=mysqli_real_escape_string($this->conn,$locality);
            $city=mysqli_real_escape_string($this->conn,$city);
            $state=mysqli_real_escape_string($this->conn,$state);
            $country=mysqli_real_escape_string($this->conn,$country);
            $status=mysqli_real_escape_string($this->conn,$status);

            $sql="INSERT INTO `property_details`(`name`, `city`, `locality`, `state`, `country`, `status`) VALUES ('$name','$city','$locality','$state','$country','$status')";   
            
            $result=mysqli_query($this->conn,$sql);
            // Validating the Result
            
            if (!$result) 
            {
                $ans=null;
                // returning null
                return $ans;
            }
            else
            {
                $ans="created";
                return $ans;
            }        
        }
        else
        {
            $ans=null;
            // returning null
            return $ans;
        }    
    }


}

?>    
