<?php

class Application_Model_Classes {

    private $class_id;
    private $class_name;
    
     

    public function __construct($class_row = NULL) {
        if (!is_null($class_row)) {
            $this->class_id = $class_row->class_id;
            $this->class_name = $class_row->class_name;
            
        }
    }

    public function __set($name, $value) {
        $this->$name = $value;
    }

    public function __get($name) {
        return $this->$name;
    }

}
