$(document).ready(function() {
    // $("video").bind("contextmenu",function(){
    //     return false;
    // });
    // $("body").bind("contextmenu",function(){
    //     // alert("Right click disabled");
    //     return false;
    // });

    $('video').on('click', function(){
        var videoStatus = $(this).get(0).paused;
        if(videoStatus == false)
        {
            $(this).get(0).pause();
        }
        else
        {
            $(this).get(0).play();
        }
    });

    $("#modal-textarea").keyup(function(event) {
        if (event.keyCode === 13) {
            var question = $("#modal-textarea").val();
            $("#serch-question").val(question);
            $("#modal-textarea").val("");
            if(question != "")
            {
                $("#text-search-form").submit();
            }
            else
            {
                $("#search-alert").show();
                setTimeout(function(){
                    $("#search-alert").hide();
                }, 5000);
            }
        }
    });

    $("#modal-question").click(function(){
        var question = $("#modal-textarea").val();
        $("#serch-question").val(question);
        $("#modal-textarea").val("");

        if(question != "")
        {
            $("#text-search-form").submit();
        }
        else
        {
            $("#search-alert").show();
            setTimeout(function(){
                $("#search-alert").hide();
            }, 5000);
        }
    });

    $("#serch-question").click(function(event){
        event.preventDefault();
        var prevQuestion = $("#serch-question").val();
        $("#modal-textarea").val(prevQuestion);
        $("#modal-textarea").focus();
    });

    $("#change-panel").click(function(){
        $(".page-content").show();
    	$("#serch-question").removeAttr("disabled");
    	$("#choose-pic-label").show();
    	$("#confirm-image").hide();
    	$("#search_question_pic").val("");
    	$(this).hide();
    	$('.rcrop-wrapper').remove();
    	$(".preview-image").remove();
    	$("#language").hide();
    	$("#submit-image").hide();
        $("#image-upload-panel").hide();
        $('<section class="col-md-9 intro" style="margin-top: 7%;"><iframe style="width: 100%; height: 450px;" src="https://www.youtube.com/embed/OWwCDp2Uzq8"></iframe></section>').insertAfter(".main-serch-container");
    });

    $("#search_question_pic").change(function(){
        $(".page-content").hide();
        $("#change-panel").show();
        $("video").get(0).pause();
     //    $(".intro").hide();
    	// $("#choose-pic-label").hide();
    	// $("#serch-question").val("");
    	// $("#serch-question").attr("disabled", "disabled");
    	// $("#match-head").remove();
    	// $(".matches-block").remove();
    });

	$(".pic_select").click(function(){
		if($("input[name='search_question_pic']").val() != "")
		{
			var path = $("input[name='search_question_pic']").val();
			alert(path);

			// $("<div class='img-container'><img id='image' src='"+path+"' alt='Picture'></div>").insertAfter(".pic_select");
		}
	});

	$('.button-section').on('click', function(){
        var videoStatus = $(this).next().find('video').get(0).paused;
        if(videoStatus == false)
        {
        	$(this).next().find('video').get(0).pause();
        }
        else
        {
        	$(this).next().find('video').get(0).play();
        }
    });

    var fixmeTop = $('.fixme').offset().top;       // get initial position of the element

	$(window).scroll(function() {                  // assign scroll event listener

	    var currentScroll = $(window).scrollTop(); // get current position

	    if (currentScroll >= fixmeTop) {           // apply position: fixed if you
	        $('.fixme').css({                      // scroll to that element or below it
	            'position': 'fixed',
	            'top': '0',
	            'z-index': '2',
	            'width': '72%',
	            'background-color': '#c8c8c8',
	            'padding': '2%'
	        });
	        $('#match-head').css({
	        	'margin-top': '20%'
	        });
	        $('#question-head').css({
	        	'margin-top': '2%'
	        });
	    } else {                                   // apply position: static
	        $('.fixme').css({                      // if you scroll above it
	            'position': 'static',
	            'width': '100%',
	            'z-index': '0',
	            'background-color': '#ffffff',
	            'padding': '0%'
	        });
	        $('#match-head').css({
	        	'margin-top': '3%'
	        });
	        $('#question-head').css({
	        	'margin-top': '7%'
	        });
	    }

	});
});

var id = 1;
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $("#image-upload-panel").show();
            if($('.preview-image').length > 0)
        	{
        		$('.preview-image').remove();
        	}
        	if($('.selected-image').length > 0)
        	{
        		var old = id - 1;
        		$('.selected-image').remove();
        		$('.rcrop-wrapper').remove();
        	}
            var image_id = "image-"+id;
            $(".image-wrapper").append("<img class='selected-image' id='"+image_id+"' src='"+e.target.result+"'>");
            $("#image-data-url").val(e.target.result);
            crop(id);
            id = id + 1;
            $("#confirm-image").show();
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function crop(id){
	$('#image-'+id).rcrop({
        grid : true,
    });

    $('#image-'+id).on('rcrop-changed rcrop-ready', function(){
	    var srcOriginal = $(this).rcrop('getDataURL');
	    $("#image-data-url").val(srcOriginal);
	    if($('.preview-image').length > 0)
    	{
    		$('.preview-image').remove();
    	}
	    $("<img class='preview-image' src='"+srcOriginal+"' style='height: 300px; width: 500px; display: none;'>").insertAfter(".demo");
	});
}

function preview()
{
	$('.selected-image').hide();
	$('.rcrop-wrapper').remove();
	$('.preview-image').show();
	$("#confirm-image").hide();
	$("#submit-image").show();
	$("#choose-file-panel").hide();
	$("#language").show();
}

function goto()
{
	var win = window.open("https://play.google.com/store/apps/details?id=com.doubtnutapp", '_blank');
	win.focus();
}
