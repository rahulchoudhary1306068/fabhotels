var isPhoneValidated = false;
var isEmailValidated = false;
var base64Image = "";
function show_box(id) {
    $('.widget-box.visible').removeClass('visible');
    $('#' + id).addClass('visible');
}
document.onreadystatechange = function () {
  var state = document.readyState
  if (state == 'complete') {
        document.getElementById('interactive');
        document.getElementById('page_loader').style.visibility="hidden";
        /*$("#otp").val("");
    document.getElementById('page_loader').style.visibility="hidden";
      document.getElementById("otp_validation").innerHTML = "OOPS! Something is not right";
      $("#otp_validation").show();
        $("#otpmodal").modal({backdrop: "static"});*/
  }
}

$(document).ready(function () {
  var navListItems = $('div.setup-panel div a'),
          allWells = $('.setup-content'),
          allNextBtn = $('.nextBtn'),
          allPrevBtn = $('.prevBtn');

  allWells.hide();


  navListItems.click(function (e) {
      e.preventDefault();
      var $target = $($(this).attr('href')),
              $item = $(this);
      if (!$item.hasClass('disabled')) {
          navListItems.removeClass('btn-primary').addClass('btn-default');
          $item.addClass('btn-primary');
          allWells.hide();
          $target.show();
          $target.find('input:eq(0)').focus();
      }
  });
  
      allPrevBtn.click(function(){
          var curStep = $(this).closest(".setup-content"),
              curStepBtn = curStep.attr("id"),
              prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

              prevStepWizard.removeAttr('disabled').trigger('click');
      });

      allNextBtn.click(function(){
      var curStep = $(this).closest(".setup-content"),
          curStepBtn = curStep.attr("id"),
          nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
          curInputs = curStep.find("input[type='text'],input[type='url'],input[type='password'],input[type='file'],select"),
          isValid = true;

      $(".form-group").removeClass("has-error");
      for(var i=0; i<curInputs.length; i++){
            console.log(curInputs[i]);
            var error_message = check_validity(curInputs[i]);
            if (error_message != ""){
              isValid = false;
              document.getElementById("validation_error").innerHTML = error_message;
              $("#validation_error").show();
              break;
              //$(curInputs[i]).closest(".form-control").addClass("has-error");
            }else{
                $("#validation_error").hide();
            }
      }

      if (isValid)
          nextStepWizard.removeAttr('disabled').trigger('click');
  });

  $('div.setup-panel div a.btn-primary').trigger('click');
});

 $( function() {
    var dobyear = (new Date).getFullYear();
    $( "#datepicker1" ).datepicker({ changeYear: true, changeMonth:true, yearRange: "1980:"+dobyear });
  } ); 

function showCollege(e){
    // $("#college2").show();
    $("#college2").css("display","block");
}

$("input").on('keyup', function(e){
    if(e.target.id != "expert_image")
    {
        $("#preview_"+e.target.id).text(e.target.value);
    }
    else
    {
        $("#preview_"+e.target.id).text(e.target.value);
    }
});

$('select').on('change', function(e) {
    if(e.target.id == "expertise_chapters"){
        $("#preview_"+e.target.id).text($("#expertise_chapters").val());
    }
    else
    {
        $("#preview_"+e.target.id).text(e.target.value);
    }
})
$('#xii_coaching').change(function(e){
  if($("#"+e.target.id).val() == "Others"){
    $("#xii_coaching_others").show();
  }
});
$('#datepicker1').change(function(e){
    $("#preview_"+e.target.id).text(e.target.value);
})
 $('#jee_attempt').change(function() {
    if (this.value == 'jee_yes') {
        $("#jee_details").show();
    }
    else if (this.value == 'jee_no') {
        $("#jee_details").hide();
        $("#preview_jee_year").text("");
        $("#preview_jee_mains_air").text("");
        $("#preview_jee_advanced_air").text("");
        $("#preview_category").text("");
    }
});

var start = 1980;
var end = new Date().getFullYear()+10;
var options = "<option value=''>select</option>";
for(var year = start ; year <=end; year++){
  options += "<option>"+ year +"</option>";
}
document.getElementById("degree1_year").innerHTML = options;
document.getElementById("degree2_year").innerHTML = options;
document.getElementById("jee_year").innerHTML = options;

var qualifications = 
  [
    "Graduate",
    "PostGradute",
    "Doctorate",
    "Diploma Holder"
  ]
var qualOptions = "<option value=''>select</option>";
for(var i = 0 ; i < qualifications.length; i++){
  qualOptions += "<option>"+ qualifications[i] +"</option>";
}
document.getElementById("degree1").innerHTML = qualOptions;
document.getElementById("degree2").innerHTML = qualOptions;


var boards = 
  [
    "CBSE",
    "ICSE",
    "State Board",
    "IB",
    "IGCSE"
  ]
var boardOptions = "<option value=''>select</option>";
for(var i = 0 ; i < boards.length; i++){
  boardOptions += "<option>"+ boards[i] +"</option>";
}
document.getElementById("xii_from").innerHTML = boardOptions;
document.getElementById("x_from").innerHTML = boardOptions;

var institutions = 
  [
    "FIITJEE",
    "VMC",
    "Resonance",
    "Allen",
    "Brilliant",
    "Pace",
    "Bansal",
    "Akash",
    "Career Point",
    "Mentors",
    "Narayana",
    "Sri Chaitanya",
    "Self Study",
    "Others",
    "No Coaching"
  ]

var instituteOptions = "<option value=''>select</option>";
for(var i = 0 ; i < institutions.length; i++){
  instituteOptions += "<option>"+ institutions[i] +"</option>";
}
document.getElementById("xii_coaching").innerHTML = instituteOptions;

var colleges = 
  [
    "Indian Institute Of Technology, Madras",
    "Indian Institute Of Technology, Bombay",
    "Indian Institute Of Technology, Kharagpur",
    "Indian Institute Of Technology, Delhi",
    "Indian Institute Of Technology, Kanpur",
    "Indian Institute Of Technology, Roorkee",
    "Indian Institute Of Technology, Hyderabad",
    "Indian Institute Of Technology, Gandhinagar",
    "Indian Institute Of Technology, Ropar-Rupnagar",
    "Indian Institute Of Technology, Patna",
    "Indian Institute Of Technology, North Guwahati",
    "National Institute Of Technology, Tiruchirappalli",
    "Vellore Institute Of Technology",
    "Indian Institute Of Technology (Banaras Hindu University), Varanasi",
    "Sardar Vallabhbhai National Institute Of Technology",
    "Indian Institute Of Technology, Indore",
    "Birla Institute Of Technology",
    "Visvesvaraya National Institute Of Technology, Nagpur (Deemed University)-Nagpur",
    "National Institute Of Technology, Rourkela-Rourkela",
    "Indian Institute Of Engineering Science And Technology, Shibpur",
    "Indian Institute Of Technology, Mandi",
    "College Of Engineering, Pune",
    "National Institute Of Technology, Karnataka-Mangalore"
  ]
var collegeOptions = "<option value=''>select</option>";
for(var i = 0 ; i < colleges.length; i++){
  collegeOptions += "<option>"+ colleges[i] +"</option>";
}
collegeOptions += "<option>Other</option>";
document.getElementById("degree1_college").innerHTML = collegeOptions;
document.getElementById("degree2_college").innerHTML = collegeOptions;

$("#degree1_college").change(function () {
  var deg1_coll = $("#degree1_college option:selected").text();
  if(deg1_coll == "Other")
  {
    $(".d1_college").css("display","block");
    $(".d1_college").attr("required","required");
  }
  else
  {
    $(".d1_college").css("display","none");
    $(".d1_college").removeAttr("required");
  }
});

$("#degree2_college").change(function () {
  var deg2_coll = $(".degree2_college option:selected").text();
  if(deg2_coll == "Other")
  {
    $(".d2_college").css("display","block");
    $(".d2_college").attr("required","required");
  }
  else
  {
    $(".d2_college").css("display","none");
    $(".d2_college").removeAttr("required");
  }
});

function check_validity(elem){
    if(elem.id == "expert_name"){
        var check = /^[A-z ]+$/.test(elem.value);
        if(!check){
            return "Please enter a valid name";
        }else{
            return "";
        }
    }else if(elem.id == "expert_email"){
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(!re.test(elem.value)){
            return "Please enter a valid email id"
        }else{
            return "";
        }
    }else if(elem.id == "hashed_password"){
        var re = /^[a-zA-Z0-9]+$/;
        if(!re.test(elem.value) || elem.value.length < 6){
            return "Please enter a 6 character password";
        }else{
            return "";
        }
    }else if(elem.id == "cpassword"){
        if(elem.value != $("#hashed_password").val()){
            return "Your passwords do not match"
        }else{
            return "";
        }
    }else if(elem.name =="expert_mobile" || elem.name == "expert_whatsapp"){
        if(elem.value.length == 10){
            return "";
        }else{
            return "Please enter a valid 10 digit number";
        }
    }else if(elem.name == "degree1_college"){
        if($(".degree1_college option:selected").text() == "select"){
            return "Please enter a valid college name";
        }else if($(".degree1_college option:selected").text() == "Other"){
            if($(".d1_college").val() == "")
              return "Please enter a valid college name";
            else if($(".degree1").val().length == 0){
                return "Please enter a valid qualification";
            }else if($(".degree1_dept").val().length == 0){
                return "Please enter a valid college department";
            }else if($("#degree1_year option:selected").text() == "select"){
                return "Please select an year";
            }else if($(".degree1_marks").val().length == 0 || isNaN($(".degree1_marks").val())){
                return "Please enter valid college marks";
            }
            else
              return "";
        }
        else if($(".degree1 option:selected").val() == "select"){
            return "Please enter a valid qualification";
        }
        // else if($(".degree1").val().length == 0){
        //     return "Please enter a valid qualification";
        // }
        else if($(".degree1_dept").val().length == 0){
            return "Please enter a valid college department";
        }else if($(".degree1_marks").val().length == 0 || isNaN($(".degree1_marks").val())){
            return "Please enter valid college marks";
        }else if($("#degree1_year option:selected").text() == "select"){
            return "Please select an year";
        }else{
            return "";
        }

    }else if(elem.name == "degree2_college" && $(".degree2_college").is(':visible')){
          // if(elem.value.length == 0){
        if($(".degree2_college option:selected").text() == "select"){
            return "Please enter a valid college name";
        }else if($(".degree2_college option:selected").text() == "Other"){
            if($(".d2_college").val() == "")
              return "Please enter a valid college name";
            else if($(".degree2").val().length == 0){
                return "Please enter a valid qualification";
            }else if($(".degree2_dept").val().length == 0){
                return "Please enter a valid college department";
            }else if($("#degree2_year option:selected").text() == "select"){
                return "Please select an year";
            }else if($(".degree2_marks").val().length == 0 || isNaN($(".degree1_marks").val())){
                return "Please enter valid college marks";
            }else
              return "";
        }
        else if($(".degree2").val().length == 0){
            return "Please enter a valid qualification";
        }else if($(".degree2_dept").val().length == 0){
            return "Please enter a valid college department";
        }else if($("#degree2_year option:selected").text() == "select"){
            return "Please select an year";
        }else if($(".degree2_marks").val().length == 0){
            return "Please enter valid college marks";
        }else{
            return "";
        }

    }else if(elem.name == "x_from"){
        if(elem.value.length == 0){
            return "Please enter a valid 10th board";
        }else{
            return "";
        }
    }else if(elem.name == "x_marks"){
        if(elem.value.length == 0 || isNaN(elem.value)){
            return "Please enter a valid marks";
        }else{
            return "";
        }

    }else if(elem.name == "xii_from"){
        if(elem.value.length == 0){
            return "Please enter a valid 12th board";
        }else{
            return "";
        }
    }else if(elem.name == "xii_marks"){
        if(elem.value.length == 0 || isNaN(elem.value)){
            return "Please enter a valid marks";
        }else{
            return "";
        }
    }else if(elem.name == "xii_coaching"){
        if(elem.value.length == 0){
            return "Please enter a valid coaching institute";
        }else{
            return "";
        }
    }else if(elem.name == "expertise_chapters"){
        if(elem.value.length == 0){
            return "Please enter valid Expertise Chapters";
        }else{
            return "";
        }
    }else if(elem.name == "jee_attempt"){
        if($('.jee_attempt').val() == "jee_yes")
        {
            if($(".jee_year option:selected").text() == "select"){
                return "Please select a valid year";
            }if($(".jee_advanced_air").val().length == 0 || isNaN($(".jee_advanced_air").val())){
                return "Please enter a valid AIR";
            }else if($(".jee_mains_air").val().length == 0 || isNaN($(".jee_mains_air").val())){
                return "Please enter a valid AIR";
            }else if($(".category option:selected").text() == "Select"){
                return "Please select your category";
            }
            else
            {
              return "";
            }
        }
        else
        {
          return "";
        }
    }
    // if($('#jee_attempt option:selected').val() == "jee_yes" && (elem.name == "jee_attempt" || elem.name == "jee_year" || elem.name == "jee_advanced_air" || elem.name == "jee_mains_air" || elem.name == "category"))
    // {
    //   alert("hi");
    // }
    // if($('#jee_attempt').val() == "jee_yes" && (elem.name == "jee_advanced_air" || elem.name == "jee_mains_air")){
    //     if($("#jee_advanced_air").val().length == 0 || isNaN($("#jee_advanced_air").val())){
    //         return "Please enter a valid AIR";
    //     }else if($("#jee_mains_air").val().length == 0 || isNaN($("#jee_mains_air").val())){
    //         return "Please enter a valid AIR";
    //     }else if(document.getElementById("category").value == ""){
    //         return "Please select your category";
    //     }else{
    //         return "";
    //     }
    // }
    /*else if(elem.name == "degree1_year" || elem.name == "degree2_year" || (elem.name =="jee_year" && $('#jee_attempt').val() == "jee_yes")){
        if(elem.value == '')
            return "Please select an year";
        else 
            return "";
    }*/else if(elem.name == "expert_image"){
        if(document.getElementById("expert_image").value == ""){
            return "Please upload your image";
        }else if(document.getElementById("expert_image").files[0].size > 524288){
            return "Please upload an image less than 500kb";
        }else
            return "";
    }else if(elem.id = "datepicker"){
        if($("#datepicker").val().length == 0){
            return "Please select your date of birth";
        }else{
            return "";
        }
    }else {
        return "";
    }

    if(!isPhoneValidated){
      return "The phone number is already taken. Please use a different Phone Number";
    }
    if(!isEmailValidated){
      return "The email id is already taken. Please use a different Email Id";
    }
}
function read_image(input){
    if (input.files && input.files[0]) {
      var reader = new FileReader();
        reader.onload = function (e) {
            $('#preview_expert_image')
                .attr('src', e.target.result)
                .width(200)
                .height(200);
            base64Image = e.target.result;
        };

        reader.readAsDataURL(input.files[0]);
    }

}

//Checking if the user has stopped typing in the phone input field
var typingTimer;                //timer identifier
var doneTypingInterval = 2000;  //time in ms, 3 second for example
var $input_phone = $('#expert_mobile');

//on keyup, start the countdown
$input_phone.on('keyup', function () {
  clearTimeout(typingTimer);
  typingTimer = setTimeout(CheckExpertPhone, doneTypingInterval);
});

//on keydown, clear the countdown 
$input_phone.on('keydown', function (e) {
  clearTimeout(typingTimer);
  if(e.which == 9){
     $('#expert_mobile').keyup();
  }
});

//Ajax call for checking if the phone number already exists
function CheckExpertPhone(){
  document.getElementById('page_loader').style.visibility="visible";
  $.ajax({
        url: "/doubtnut/public/admin/ajax/iexpert-mobile-exist",
        method: "POST",        
        data: {"expert_mobile": $("#expert_mobile").val()},
        error: function(data,errMsg) {
              document.getElementById('page_loader').style.visibility="hidden";
              document.getElementById("validation_error").innerHTML = "OOPS! Something is not right";
              $("#validation_error").show();
          },
        success: function(data){
            document.getElementById('page_loader').style.visibility="hidden";
            if(data.meta.code == 200){
              document.getElementById("validation_error").innerHTML = "The phone number is already taken. Please use a different Phone Number";
              isPhoneValidated = false;
              $("#validation_error").show();
            }else{
              isPhoneValidated = true;
              $("#validation_error").hide();
            }

          }
    });
}

//Checking if the user has stopped typing in the email input field
var typingTimerEmail;                //timer identifier
var doneTypingIntervalEmail = 2000;  //time in ms, 3 second for example
var $input_email = $('#expert_email');

//on keyup, start the countdown
$input_email.on('keyup', function () {
  clearTimeout(typingTimerEmail);
  typingTimerEmail = setTimeout(CheckExpertEmail, doneTypingIntervalEmail);
});

//on keydown, clear the countdown 
$input_email.on('keydown', function (e) {
  clearTimeout(typingTimerEmail);
  if(e.which == 9){
     $('#expert_email').keyup();
  }
});

//Ajax call for checking if the phone number already exists
function CheckExpertEmail(){
  document.getElementById('page_loader').style.visibility="visible";
  $.ajax({
        url: "/doubtnut/public/admin/ajax/iexpert-email-exist",
        method: "POST",        
        data: {"expert_email": $("#expert_email").val()},
        error: function(data,errMsg) {
              document.getElementById('page_loader').style.visibility="hidden";
              document.getElementById("validation_error").innerHTML = "OOPS! Something is not right";
                $("#validation_error").show();
          },
        success: function(data){
            document.getElementById('page_loader').style.visibility="hidden";
            if(data.meta.code == 200){
              document.getElementById("validation_error").innerHTML = "The email id is already taken. Please use a different Email Id";
              isEmailValidated =  false;
              $("#validation_error").show();
            }else{
              isEmailValidated = true;
              $("#validation_error").hide();
            }

          }
    });
}

function SubmitSignUpForm(){
  document.getElementById('page_loader').style.visibility="visible";
  var requestData = {};
  requestData["expert_name"] = $("#expert_name").val();
  requestData["expert_email"] = $("#expert_email").val();
  requestData["hashed_password"] = $("#hashed_password").val();
  requestData["expert_dob"] = $("#datepicker").val();
  requestData["expert_mobile"] = $("#expert_mobile").val();
  requestData["expert_whatsapp"] = $("#expert_whatsapp").val();
  requestData["expert_img"] = base64Image;
  if($(".degree1_college").text() != "Other")
  {
    requestData["degree1_college"] = $(".degree1_college").val();
  }
  else
  {
    requestData["degree1_college"] = $(".d1_college").val();
  }
  // requestData["degree1_college"] = $("#degree1_college").val();
  requestData["degree1"] = $(".degree1").val();
  requestData["degree1_dept"] = $(".degree1_dept").val();
  requestData["degree1_year"] = $(".degree1_year").val();
  requestData["degree1_marks"] = $(".degree1_marks").val();
  // requestData["degree2_college"] = $("#degree2_college").val();
  if($(".degree2_college").text() != "Other")
  {
    requestData["degree2_college"] = $(".degree2_college").val();
  }
  else
  {
    requestData["degree2_college"] = $(".d2_college").val();
  }
  requestData["degree2"] = $(".degree2").val();
  requestData["degree2_dept"] = $(".degree2_dept").val();
  requestData["degree2_year"] = $(".degree2_year").val();
  requestData["degree2_marks"] = $(".degree2_marks").val();
  requestData["x_from"] = $("#x_from").val();
  requestData["x_marks"] = $("#x_marks").val();
  requestData["xii_from"] = $("#xii_from").val();
  requestData["xii_marks"] = $("#xii_marks").val();
  requestData["xii_coaching"] =$("#xii_coaching").val();
  requestData["expertise_chapters"] = $("#expertise_chapters").val().toString();
  if($('.jee_attempt').val() == "jee_yes")
  {
    requestData["jee_year"] =$(".jee_year").val();
    requestData["jee_cat"] = $(".category").val();
    requestData["jee_advanced_air"]= $(".jee_advanced_air").val();
    requestData["jee_mains_air"] = $(".jee_mains_air").val();
  }
  // console.log(requestData);
  // return false;
  $.ajax({
        url: "/doubtnut/public/iexperts/auth/signup-api",
        method: "POST",        
        data: requestData,
        error: function(data,errMsg) {
              document.getElementById('page_loader').style.visibility="hidden";
              document.getElementById("validation_error").innerHTML = "OOPS! Something is not right";
                $("#validation_error").show();
          },
        success: function(data){
            document.getElementById('page_loader').style.visibility="hidden";
            if(data.meta.code == 200){
              //Open otp modal
              $("#otpmodal").modal({backdrop: "static"});
            }else{
              document.getElementById('page_loader').style.visibility="hidden";
                document.getElementById("validation_error").innerHTML = "OOPS! Something is not right";
                  $("#validation_error").show();
            }
          }
    });
}

function VerifyOtp(){
  if($("#otp").val().length == 0){
    $("#otp_validation").hide();
    $("#otp_validation_success").hide();
    $("#otp_resend_success").hide();
    document.getElementById("otp_validation").innerHTML = "Please enter a valid OTP";
      $("#otp_validation").show();
  }else{
    $("#otp_validation").hide();
    $("#otp_validation_success").hide();
    $("#otp_resend_success").hide();
    document.getElementById('page_loader').style.visibility="visible";
    $.ajax({
          url: "/doubtnut/public/iexperts/auth/otp-verification-api",
          method: "POST",        
          data: {"expert_mobile": $("#expert_mobile").val(), "expert_email": $("#expert_email").val(), "verification_code": $("#otp").val(), "request_type":"verify"},
          error: function(data,errMsg) {
                document.getElementById('page_loader').style.visibility="hidden";
                document.getElementById("otp_validation").innerHTML = "Please enter a valid OTP";
            $("#otp_validation").show();
            },
          success: function(data){
              document.getElementById('page_loader').style.visibility="hidden";
              if(data.meta.code == 200){
                $("#otp").val("");
                $(".modal-title").hide();
                $("#otp").hide();
                $("#resend_btn").hide();
                $("#verify_btn").hide();
                $("#otp_validation_success").show();
              }else{
                document.getElementById('page_loader').style.visibility="hidden";
                  document.getElementById("otp_validation").innerHTML = "Please enter correct OTP.";
                    $("#otp_validation").show();
              }
            }
      });
  }
}

function ResendOtp(){
  $("#otp_validation").hide();
  $("#otp_validation_success").hide();
  $("#otp_resend_success").hide();
  document.getElementById('page_loader').style.visibility="visible";
  $.ajax({
        url: "/doubtnut/public/iexperts/auth/resend-verification-code-api",
        method: "POST",        
        data: {"expert_mobile": $("#expert_mobile").val(), "expert_email": $("#expert_email").val(), "request_type":"verify"},
        error: function(data,errMsg) {
              document.getElementById('page_loader').style.visibility="hidden";
              document.getElementById("otp_validation").innerHTML = "OOPS! Something is not right";
                $("#otp_validation").show();
          },
        success: function(data){
            document.getElementById('page_loader').style.visibility="hidden";
            if(data.meta.code == 200){
              $("#otp_resend_success").show();
            }else{
              document.getElementById('page_loader').style.visibility="hidden";
                document.getElementById("otp_validation").innerHTML = "OOPS! Something is not right";
                  $("#otp_validation").show();
            }
          }
    });
}




