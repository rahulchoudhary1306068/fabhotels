<?php

class My_Categories {
    
    
    public function allCategories(){
        
        $categoriesMapper = new Application_Model_CategoriesMapper();
        $categories = $categoriesMapper->getAllCategories();
        
        $menuitemsMapper = new Application_Model_MenuItemsMapper();
        $count_arr = array(); 
        foreach ($categories as $category){
            $category_id = $category->__get("category_id");
            $menuitems = $menuitemsMapper->getMenuItemsByCategoryId($category_id);
            
            $count_arr[] = count($menuitems);
        }
        
        return $count_arr;
    }
}