<?php

class My_AllData {
   
    
    public function getAllFranchies(){
        
        $franchiseMapper = new Application_Model_FranchiseMapper();
        $franchises = $franchiseMapper->getAllFranchises();
        
        return $franchises;
        
    }
}