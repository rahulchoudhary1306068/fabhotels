<?php

class My_ApiCommon extends Zend_Controller_Action {

    protected $_params=null;
    protected $_objJSON = null;
    protected $translate = null;
    protected $_auth = null;
    protected $_paypal = null;

    public function errorMessage($lang = 'en') {
        $this->translate = new Zend_Translate(
                                array(
                                    'adapter' => 'array',
                                    'content' => APPLICATION_PATH.'/configs/languages/'
                                        .$lang.'.inc.php',
                                    'locale' => $lang
                                )
                            );    
    }

    public function sendRequest($regids,$message) {
/*       
        $result = $this->celery->PostTask('task2.tasks.notify_answer_done', array($regids, $message, "Doubtnut Update"));
*/
      define("GOOGLE_API_KEY", "AIzaSyCLQbnBMNcuqmR7CuUw49oKIbXL_Yuezjw"); 
                // Set POST variables
        //$message = array("price" => $message);
        $url = 'https://fcm.googleapis.com/fcm/send';

        $reg = [$regids];
        $data = [$message];
        $fields = array(
            'registration_ids' => $reg,
            'data' => $message
            );
        $headers = array(
            //'Authorization: key=AIzaSyAXu2kaXxlwcRDMKd4qBinskEvsXV18FrM',
           'Authorization: key='.GOOGLE_API_KEY.'',
            'Content-Type: application/json'
        );
//  /print_r($fields);exit;
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        // Close connection
        curl_close($ch);

        return $result;
    }

    public function getRequestParams() {
        $this->errorMessage();
            if($this->getRequest()->isPost()) {
                $body = $this->getRequest()->getRawBody();

                $this->_params = json_decode($body, true);
                if ($body && json_last_error() !== JSON_ERROR_NONE) {
                    $this->_params = $this->getRequest()->getParams();
                }  
                
                if ( isset($this->_params['data']) ) {
                    $data = base64_decode($this->_params['data']);
                    $this->_params = @json_decode($data , true);
                    if (json_last_error() !== JSON_ERROR_NONE) {
                        $this->_params = array();
                    }
                }
                if(! isset($this->_params) || count($this->_params)==0) {
                    $this->_params = $_POST;
                }   
            } else {

            }
            $response = $this->_objJSON->encode(array('meta' => array('code'=>BAD_REQUEST_CODE, 'message'=>$this->translate->translate('BAD_REQUEST'))));        
    }

    public function checkAccessToken($student_id = false) {
        if ($access_token && $this->_auth->getIdentity()) {        
            $dec = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, SECRET_KEY, base64_decode($access_token), MCRYPT_MODE_ECB);
            $tokens = explode(":", $dec);
            $udid = $tokens[2];
            $studentsMapper = new Application_Model_StudentsMapper();
            $student = $studentsMapper->getStudentById($tokens[1]);
            if ($tokens[0] > strtotime("Today") && strpos($tokens[1],$this->_auth->getIdentity()->student_id) !== false) {
                if($student && $udid == $student->__get("udid")){
                    return true;
                }else{
                    return false;
                }
            }
        } else {
            return false;
        }
    }

    public function checkAccessToken1($student_id = false) {
        $request = new Zend_Controller_Request_Http();
        $access_token = $request->getHeader('access_token');
        if ($access_token) {        
            $dec = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, SECRET_KEY, base64_decode($access_token), MCRYPT_MODE_ECB);
            $tokens = explode(":", $dec);
            $studentsMapper = new Application_Model_StudentsMapper();
            $student = $studentsMapper->getStudentById($tokens[1]);
            if ($tokens[0] > strtotime("Today") && $student) {
                return $tokens[1];
            }
        } else {
            return false;
        }
    }

    public function checkAccessToken2(){
         $request = new Zend_Controller_Request_Http();
        $access_token = $request->getHeader('access_token');
        if ($access_token && $this->_auth->getIdentity()) {        
            $dec = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, SECRET_KEY, base64_decode($access_token), MCRYPT_MODE_ECB);
            $tokens = explode(":", $dec);
            $udid = $tokens[2];
            $studentsMapper = new Application_Model_StudentsMapper();
            $student = $studentsMapper->getStudentById($tokens[1]);
            if($student->__get("app_version")  < "4.2.3"){
                if ($tokens[0] > strtotime("Today") && strpos($tokens[1],$this->_auth->getIdentity()->student_id) !== false) {
                    return true;
                }else{
                    return false;
                }
            }else{
                if($student && $udid == $student->__get("udid")){
                    return true;
                }else{
                    return false;
                }
            }
        } else {
            return false;
        }
    }



    public function calculateEndSubsDateTillApril($scheme_validity) {

        if ($scheme_validity == 'yearly') {
            $plus_one = strtotime("+1 Year");
            $class_end = strtotime("30 April ".strval(date("Y", strtotime("today"))+1));
            $time = $plus_one > $class_end? $class_end: $plus_one; 
            $end_date = date("Y-m-d",$time);
            //$question_to_be_asked = 0;
        } else if($scheme_validity == 'monthly') {
            $plus_one = strtotime("+1 Month");
            $class_end = strtotime("30 April ".strval(date("Y", strtotime("today"))+1));
            $time = $plus_one > $class_end? $class_end: $plus_one; 
            $end_date = date("Y-m-d",$time);
            //$question_to_be_asked = 0;
        } else if($scheme_validity == 'quarterly') {
            $plus_one = strtotime("+3 Months");
            $class_end = strtotime("30 April ".strval(date("Y", strtotime("today"))+1));
            $time = $plus_one > $class_end? $class_end: $plus_one; 
            $end_date = date("Y-m-d",$time);
            //$question_to_be_asked = 0;
        } else {
            $end_date = date("Y-m-d",strtotime("+1 Week"));
           //$question_to_be_asked = intval($scheme_questions);
        }
        return $end_date;        
    }

    public function calculateEndSubsDate($scheme_validity, $end_date) {
        $today = strtotime("today");
        $diff = $end_date > $today ? $end_date - $today : 0;
        if ($scheme_validity == 'yearly') {
            $plus_one = strtotime("+1 Year");
            $time = $plus_one ; 
        } else if($scheme_validity == 'monthly') {
            $plus_one = strtotime("+1 Month");
            $time = $plus_one ; 
        } else if($scheme_validity == 'quarterly') {
            $plus_one = strtotime("+3 Months");            
            $time = $plus_one ; 
        } else {
            $plus_one = strtotime("+".$scheme_validity);
            $time = $plus_one ;
        }
        $result = date("Y-m-d",$time+$diff);
        return $result;     
    }

    public function sendMessageByTwillio( $user_mobile_no , $message) 
    { 
        $sid = Zend_Registry::getInstance()->cns->twilio->sid;
        $token = Zend_Registry::getInstance()->cns->twilio->authtoken;
        $number = Zend_Registry::getInstance()->cns->twilio->number;
        
        $client = new Services_Twilio($sid, $token);

        
        $status = $client->account->sms_messages->create($number, $user_mobile_no, $message, array());
        $st = $client->get_error_msg();

        $twillio_err = ($st[1]==400) ? ($st[0]) : '';
        if( $twillio_err =='' ){

        }
        return $twillio_err;
                
    }

}