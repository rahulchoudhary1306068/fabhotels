<?php

class My_Mail {

    public function __construct() {
    }

    public function sendMail($email, $subject, $message){
        try {
             $config = array('ssl' => MAIL_SSL,
                'auth' => MAIL_AUTH,
                'username' => MAIL_USERNAME,
                'port' => MAIL_PORT,
                'password' => MAIL_PASSWORD);

            $transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
            $mail = new Zend_Mail();
            $mail->setBodyHtml($message);
            $mail->setFrom(MAIL_USERNAME, "Doubtnut");
            $mail->addTo($email, 'recipient');
            $mail->setSubject($subject);
            $mail->send($transport);
            return true;
        } catch (Exception $e) {
            return false;
        }

    }
    
}