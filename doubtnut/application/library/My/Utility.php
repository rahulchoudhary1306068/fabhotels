<?php
use Google\Cloud\Vision\VisionClient;


class Utility
{
  public static function makeUrlRequest($url, $method = 'GET', $postFields = [], $headers = [])
  {
    $curl = curl_init();
    $options = array(
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => $method,
      CURLOPT_POSTFIELDS => $postFields,
      CURLOPT_HTTPHEADER => $headers,
    );
    curl_setopt_array($curl, $options);

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      throw new Exception("cURL Error #:" . $err);
    } else {
      return $response;
    }
  }
  public static function sendNotification($regid, $message){
    if (!defined('GOOGLE_API_KEY')) define("GOOGLE_API_KEY", "AIzaSyCLQbnBMNcuqmR7CuUw49oKIbXL_Yuezjw");
    $url = 'https://fcm.googleapis.com/fcm/send';
    $fields = array(
      'to' => $regid,
      'data' => $message
    );
    $headers = array(
      //'Authorization: key=AIzaSyAXu2kaXxlwcRDMKd4qBinskEvsXV18FrM',
      'Authorization: key='.GOOGLE_API_KEY.'',
      'Content-Type: application/json'
    );
//  /print_r($fields);exit;
    // Open connection
    $ch = curl_init();

    // Set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $url);

    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    // Disabling SSL Certificate support temporarly
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

    // Execute post
    $result = curl_exec($ch);
    if ($result === FALSE) {
      die('Curl failed: ' . curl_error($ch));
    }
    // Close connection
    curl_close($ch);

    return $result;

  }
  public static function sendRequest($regids, $message)
  {
    /*
            $result = $this->celery->PostTask('task2.tasks.notify_answer_done', array($regids, $message, "Doubtnut Update"));
    */
    if (!defined('GOOGLE_API_KEY')) define("GOOGLE_API_KEY", "AIzaSyCLQbnBMNcuqmR7CuUw49oKIbXL_Yuezjw");
    // Set POST variables
    //$message = array("price" => $message);
    $url = 'https://fcm.googleapis.com/fcm/send';

    $reg = [$regids];
    $data = [$message];
    $fields = array(
      'registration_ids' => $reg,
      'data' => $message
    );
    $headers = array(
      //'Authorization: key=AIzaSyAXu2kaXxlwcRDMKd4qBinskEvsXV18FrM',
      'Authorization: key=' . GOOGLE_API_KEY . '',
      'Content-Type: application/json'
    );
//  /print_r($fields);exit;
    // Open connection
    $ch = curl_init();

    // Set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $url);

    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    // Disabling SSL Certificate support temporarly
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

    // Execute post
    $result = curl_exec($ch);
    if ($result === FALSE) {
      die('Curl failed: ' . curl_error($ch));
    }
    // Close connection
    curl_close($ch);

    return $result;
  }

  public static function upload_to_vdo($title)
  {
    //upload to vdo
    $responseObj = json_decode(self::makeUrlRequest(
      "https://dev.vdocipher.com/api/videos?title=$title",
      'PUT',
      [],
      [
        "authorization:  Apisecret " . VDO_SECRET,
        "cache-control: no-cache",
      ]
    ));
    if (isset($responseObj->videoId) && $responseObj->videoId) {
      $file_name_with_full_path = PUBLIC_PATH . "/public/question_video/" . $title;
      defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(dirname(dirname(dirname(dirname(__FILE__)))))));

      if (function_exists('curl_file_create')) { // php 5.5+

        $cFile = curl_file_create($file_name_with_full_path);
      } else { //
        $cFile = '@' . realpath($file_name_with_full_path);
      }
      $uploadResponse = self::makeUrlRequest($responseObj->clientPayload->uploadLink, 'POST', [
        'policy' => $responseObj->clientPayload->policy,
        'key' => $responseObj->clientPayload->key,
        'success_action_status' => 201,
        'success_action_redirect' => '',
        'x-amz-date' => $responseObj->clientPayload->{'x-amz-date'},
        'x-amz-algorithm' => $responseObj->clientPayload->{'x-amz-algorithm'},
        'x-amz-credential' => $responseObj->clientPayload->{'x-amz-credential'},
        'x-amz-signature' => $responseObj->clientPayload->{'x-amz-signature'},
        'file' => $cFile,
      ]);
      return $responseObj->videoId;
    } else {
      die('Vdo upload fail');
      exit;
    }
  }
  public static function imageUpload($input_name, $file_prefix = "file", $q_id) {
    $adapter = new Zend_File_Transfer_Adapter_Http();
    //$adapter->addValidator('Extension', false, 'jpg,png,gif');

    defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(dirname(dirname(dirname(__FILE__))))));
    //define('UPLOAD_DIR', '/var/www/html/doubtnut');

    $files = $adapter->getFileInfo();

    $uniqId = time();
//print_r(PUBLIC_PATH);
//exit;
    $newFilename = null;
    foreach ($files as $file => $info) {
      if ($file == $input_name and strlen($info["name"]) > 0) {
        $adapter->setDestination(PUBLIC_PATH . "/public/question_video/");
        $originalFilename = pathinfo($adapter->getFileName($file));
        //$extesion = $file["extension"];
        $newFilename = $file_prefix . '-' . $uniqId . '.' . $originalFilename['extension'];
        $adapter->addFilter('Rename', $newFilename, $file);
        $adapter->receive($file);
      }
      return $newFilename;
    }
  }

  public static function newImageUpload($file_prefix = "file", $q_id) {
    $adapter = new Zend_File_Transfer_Adapter_Http();
    //$adapter->addValidator('Extension', false, 'jpg,png,gif');
    $allowedImageTypes=array('jpg','jpeg','JPEG','gif','png');
    $allowedVideoTypes=array('mp4');
    defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(dirname(dirname(dirname(dirname(__FILE__))))));
    //define('UPLOAD_DIR', '/var/www/html/doubtnut');
    $files = $adapter->getFileInfo();
    $uniqId = time();
  
    $newFilenameContainer = array();
    foreach ($files as $file => $info) {
      if(strlen($info["name"]) > 0) {
        
        
        $originalFilename = pathinfo($adapter->getFileName($file));
        $extension = $originalFilename["extension"];
        
        if($extension!='mp4' && in_array($extension, $allowedImageTypes))
        {
          $adapter->setDestination(PUBLIC_PATH . "/public/question_image/");
          $newFilename=$q_id.'.'.$originalFilename['extension'];
          $newFilenameContainer["image"]=$newFilename;
        }
        else if($extension=='mp4' && in_array($extension, $allowedVideoTypes))
        {
          $adapter->setDestination(PUBLIC_PATH . "/public/question_video/");
          $newFilename = "answer" . '-' . $uniqId . '.' . $originalFilename['extension'];
          $newFilenameContainer["video"]=$newFilename;
        }
        
        
        $adapter->addFilter('Rename', $newFilename, $file);
        $adapter->receive($file);

      }
      
    }
    print_r($newFilenameContainer);
    return $newFilenameContainer;
  }

  public static function smsNotification($number, $sms)
  {
    $number = substr($number, 0, 10);
    $sms = urlencode($sms);
    //$url = "http://103.241.136.228/smsapi/pushsms.aspx?user=class21a&pwd=tararumpum!123&to=91" . $number . "&sid=DOUNUT&msg=" . $sms . "&fl=0&gwid=2";
    //$url = "https://control.msg91.com/api/sendhttp.php?authkey=121149AcVofMEFkE579f5762&mobiles=91".$number."&message=".$sms."&sender=DOUBTS&route=4&country=91";
    //$text = file_get_contents($url);
    $url = "http://cp.smsgatewayhub.com/SendSMS/sendmsg.php?uname=doubtnut&pass=sms@123&send=DOUBTS&dest=91" . $number . "&msg=" . $sms;
    $text = file_get_contents($url);
    return $text;
  }


  public static function vision_api($question_image, $detection){
    if (!defined('USER_QUESTION_PATH')) define("USER_QUESTION_PATH", realpath(dirname(dirname(dirname(dirname(__FILE__)))))."/public/question_image/");
    putenv('GOOGLE_APPLICATION_CREDENTIALS='.GOOGLE_APPLICATION_CREDENTIALS);
    $projectId = GOOGLE_PROJECT_ID;
    $vision = new VisionClient([
      'projectId' => $projectId,
    ]);
//    print_r(USER_QUESTION_PATH.$question_image);
//    exit;
    $image = $vision->image(file_get_contents(USER_QUESTION_PATH.$question_image), [$detection]);
    $result = $vision->annotate($image);
    $res = (array) $result->text();
    $size = sizeof($res);
    if($size == 0){
      return '';
    }else{
      return  $result->text()[0]->description();
    }
  }
  public static function mathpix_ocr($question_image){
    if (!defined('USER_QUESTION_PATH')) define("USER_QUESTION_PATH", realpath(dirname(dirname(dirname(dirname(__FILE__)))))."/public/question_image/");
    $client = new Zend_Http_Client(MATHPIX_ENDPOINT);
    $client->setConfig(
      array(
        'strict' => false
      )
    );
    $client->setHeaders('app_id',MATHPIX_APP_ID);
    $client->setHeaders('app_key',MATHPIX_APP_KEY);
    $client->setHeaders('Content-type','application/json');
    $url = USER_QUESTION_PATH.$question_image;
    $formats = array(
      "mathml"=> true
    );
    $data = array(
      "url"=>$url,
      "formats"=>$formats
    );
    $objJSON = new Zend_Json();

    $result1 = $client->setRawData($objJSON->encode($data), 'application/json')->request('POST');
    $body =  json_decode($result1->getBody());
    $responseArray = (array)$body;
    if( isset($responseArray['latex']) &&  strlen($responseArray['latex']) > 0){
      $client = new Zend_Http_Client(LATEX_TO_ASCII_CONVERSION);
      $client->setParameterPost(array(
        'latex'  => $responseArray['latex']
      ));
      $response = $client->request('POST');
      return $response->getBody();
    }else{
      return "";
    }
  }
  

   public static function createDeepLink($campaign,$channel,$feature,$stage=null,$tags=null,$alias=null,$qid=null,$student_id=null){
    $payload = array(
      'branch_key' => BRANCH_SECRET,
      'campaign' => $campaign,
      'channel' => $channel,
      'feature' => $feature,
      'type' => '2'
    );
    if(isset($qid) || isset($sid)){
      $payload['data'] = array();
    }
    if(isset($qid)){
      $payload['data']['qid'] = $qid;
    }
    if(isset($tags)){
      $payload['tags'] = $tags;
    }
    // if(isset($alias)){
    //   $payload['alias'] = $alias;
    // }
    if(isset($stage)){
      $payload['stage'] = $stage;
    }
    
    if(isset($student_id)){
      $payload['data']['sid'] = $student_id;
    }
    $response = self::makeUrlRequest('https://api.branch.io/v1/url','POST',json_encode($payload));
    return $response;
  }



      



}
