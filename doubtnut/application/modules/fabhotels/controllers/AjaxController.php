<?php

class Fabhotels_AjaxController extends Zend_Controller_Action {

	public function init()
	{
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
        $this->_objJSON = new Zend_Json();
	}
    
    public function getHotelsAction()
    {
    	try
    	{
            $request=$this->getRequest();
            //Getting the search key
            $searchKey=$request->getParam("search_key");
            $fabhotelsMapper = new Application_Model_FabhotelsMapper();
            
            $result = $fabhotelsMapper->getHotels($searchKey);
            

            if(count($result)>0)
            {
                $response = $this->_objJSON->encode(array('meta' => array('code'=>200, 'message'=>"Results set non empty.",'success' => true),'data' => $result));	
            }
            else
            {
            	$response = $this->_objJSON->encode(array('meta' => array('code'=>200, 'message'=>"Result set empty.",'success' => true),'data' => array()));
            }
        }
        catch (Exception $ex) 
        {
            
            $response = $this->_objJSON->encode(array('meta' => array('code'=>403, 'message'=>$e->getMessage()),'success' => false,'data' => null));
        }

         $this->getResponse()->setHeader('Content-Type', 'application/json')->appendBody($response);
    }


    public function getHotelAction()
    {
    	try
    	{
            $request=$this->getRequest();
            //Getting the search key
            $hotel_id=$request->getParam("id");
            $fabhotelsMapper = new Application_Model_FabhotelsMapper();
            
            $result = $fabhotelsMapper->getHotel($hotel_id);
            

            if(count($result)>0)
            {
                $response = $this->_objJSON->encode(array('meta' => array('code'=>200, 'message'=>"Results set non empty.",'success' => true),'data' => $result));	
            }
            else
            {
            	$response = $this->_objJSON->encode(array('meta' => array('code'=>200, 'message'=>"Result set empty.",'success' => true),'data' => array()));
            }
        }
        catch (Exception $ex) 
        {
            
            $response = $this->_objJSON->encode(array('meta' => array('code'=>403, 'message'=>$e->getMessage()),'success' => false,'data' => null));
        }

         $this->getResponse()->setHeader('Content-Type', 'application/json')->appendBody($response);
    }

    public function updateHotelAction()
    {
    	try
    	{
            $request=$this->getRequest();
            //Getting the search key
            $id=$request->getParam("id");
            $name=$request->getParam("name");
            $locality=$request->getParam("locality");
            $city=$request->getParam("city");
            $state=$request->getParam("state");
            $country=$request->getParam("country");
            $status=$request->getParam("status");

            $fabhotelsMapper = new Application_Model_FabhotelsMapper();
            
            $result = $fabhotelsMapper->updateHotel($id,$name,$locality,$city,$state,$country,$status);
            

            if($result=="updated")
            {
                $response = $this->_objJSON->encode(array('meta' => array('code'=>200, 'message'=>"Results set non empty.",'success' => true),'data' => $result));	
            }
            else
            {
            	$response = $this->_objJSON->encode(array('meta' => array('code'=>200, 'message'=>"Result set empty.",'success' => true),'data' => array()));
            }
        }
        catch (Exception $ex) 
        {
            
            $response = $this->_objJSON->encode(array('meta' => array('code'=>403, 'message'=>$e->getMessage()),'success' => false,'data' => null));
        }

         $this->getResponse()->setHeader('Content-Type', 'application/json')->appendBody($response);
    }

    public function deleteHotelAction()
    {
    	try
    	{
            $request=$this->getRequest();
            //Getting the search key
            $hotel_id=$request->getParam("id");
            $fabhotelsMapper = new Application_Model_FabhotelsMapper();
            
            $result = $fabhotelsMapper->deleteHotel($hotel_id);
            

            if($result=="deleted")
            {
                $response = $this->_objJSON->encode(array('meta' => array('code'=>200, 'message'=>"Results set non empty.",'success' => true),'data' => $result));	
            }
            else
            {
            	$response = $this->_objJSON->encode(array('meta' => array('code'=>200, 'message'=>"Result set empty.",'success' => true),'data' => array()));
            }
        }
        catch (Exception $ex) 
        {
            
            $response = $this->_objJSON->encode(array('meta' => array('code'=>403, 'message'=>$e->getMessage()),'success' => false,'data' => null));
        }

         $this->getResponse()->setHeader('Content-Type', 'application/json')->appendBody($response);
    }
}

?>