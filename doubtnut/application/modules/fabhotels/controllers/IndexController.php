<?php 

class Fabhotels_IndexController extends Zend_Controller_Action {

	public function init()
	{
		$this->_helper->layout()->setLayout('fabhotels');
	}

	public function indexAction(){
			$this->view->headerText="FabHotels Search Engine:";
	}

	public function createAction(){
			$this->view->headerText="FabHotels | Create a record:";
			$this->view->notice="";
			$this->view->success = null;
			$request=$this->getRequest();
			if($request)
			{
				//Getting the search key
	            $name=$request->getParam("hotel_name");
	            $locality=$request->getParam("locality");
				$city=$request->getParam("city");
				$state=$request->getParam("state");
				$country=$request->getParam("country");
				$status=$request->getParam("status");

				//var_dump($status);die;
				$fabhotelsMapper = new Application_Model_FabhotelsMapper();
	            
	            $result = $fabhotelsMapper->createHotel($name,$locality,$city,$state,$country,$status);


	            if($result=="created"){
	            	$this->view->notice = "Record Successfully Created";
	            	$this->view->success = true;
	            }

	            else{
	            	$this->view->notice = "Record Successfully not Created";
	            	$this->view->success = false;
	            }
			}
            

	}

	public function getAction(){
			$this->view->headerText="FabHotels | All records:";
			$fabhotelsMapper = new Application_Model_FabhotelsMapper();
			$hotels = $fabhotelsMapper->getAllHotels();
			$this->view->hotels = $hotels;
	}

	

}

?>