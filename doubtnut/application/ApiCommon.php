<?php

class My_ApiCommon extends Zend_Controller_Action {

    protected $_params=null;
    protected $_objJSON = null;
    protected $translate = null;
    protected $_auth = null;


    public function errorMessage($lang = 'en') {
        $this->translate = new Zend_Translate(
                                array(
                                    'adapter' => 'array',
                                    'content' => APPLICATION_PATH.'/configs/languages/'
                                        .$lang.'.inc.php',
                                    'locale' => $lang
                                )
                            );    
    }

    public function getRequestParams() {    
        $this->errorMessage();
            if($this->getRequest()->isPost()) {
                $body = $this->getRequest()->getRawBody();

                $this->_params = json_decode($body, true);
                if ($body && json_last_error() !== JSON_ERROR_NONE) {
                    $this->_params = $this->getRequest()->getParams();
                }  
                
                if ( isset($this->_params['data']) ) {
                    $data = base64_decode($this->_params['data']);
                    $this->_params = @json_decode($data , true);
                    if (json_last_error() !== JSON_ERROR_NONE) {
                        $this->_params = array();
                    }
                }
                if(! isset($this->_params) || count($this->_params)==0) {
                    $this->_params = $_POST;
                }   
            } else {

            }
            $response = $this->_objJSON->encode(array('meta' => array('code'=>BAD_REQUEST_CODE, 'message'=>$this->translate->translate('BAD_REQUEST'))));        
    }

    public function checkAccessToken($student_id = false) {
        $request = new Zend_Controller_Request_Http();
        $access_token = $request->getHeader('access_token');
        if ($access_token && $this->_auth->getIdentity()) {        
            $dec = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, SECRET_KEY, base64_decode($access_token), MCRYPT_MODE_ECB);
            if ($tokens[0] > strtotime("Today") && strpos($tokens[1],$this->_auth->getIdentity()->student_id) !== false) {
                return true;
            }
        } else {
            return false;
        }
    }

    public function calculateEndSubsDateTillApril($scheme_validity) {

        if ($scheme_validity == 'yearly') {
            $plus_one = strtotime("+1 Year");
            $class_end = strtotime("30 April ".strval(date("Y", strtotime("today"))+1));
            $time = $plus_one > $class_end? $class_end: $plus_one; 
            $end_date = date("Y-m-d",$time);
            //$question_to_be_asked = 0;
        } else if($scheme_validity == 'monthly') {
            $plus_one = strtotime("+1 Month");
            $class_end = strtotime("30 April ".strval(date("Y", strtotime("today"))+1));
            $time = $plus_one > $class_end? $class_end: $plus_one; 
            $end_date = date("Y-m-d",$time);
            //$question_to_be_asked = 0;
        } else if($scheme_validity == 'quarterly') {
            $plus_one = strtotime("+3 Months");
            $class_end = strtotime("30 April ".strval(date("Y", strtotime("today"))+1));
            $time = $plus_one > $class_end? $class_end: $plus_one; 
            $end_date = date("Y-m-d",$time);
            //$question_to_be_asked = 0;
        } else {
            $end_date = date("Y-m-d",strtotime("+1 Week"));
           //$question_to_be_asked = intval($scheme_questions);
        }
        return $end_date;        
    }

    public function calculateEndSubsDate($scheme_validity) {
        
        if ($scheme_validity == 'yearly') {
            $plus_one = strtotime("+1 Year");
            $time = $plus_one ; 
        } else if($scheme_validity == 'monthly') {
            $plus_one = strtotime("+1 Month");
            $time = $plus_one ; 
        } else if($scheme_validity == 'quarterly') {
            $plus_one = strtotime("+3 Months");            
            $time = $plus_one ; 
        } else {
            $plus_one = strtotime("+1 Week");
            $time = $plus_one ;
        }
        $result = date("Y-m-d",$time);
        return $result;     
    }
}